# Related Key Forgery Attacks on TinyJambu
This repository contains code to verify the attacks described in the paper on
Practical Related-Key Forgery attacks on TinyJambu (eprint 2022/1122). It uses
the reference implementation supplied by the designers.

## Building
Run `make` in the `code` directory and then run `find_forgery_256.out` to find
a forgery on the 256-bit version. The code will take some time to complete as
it uses the reference implementation.


